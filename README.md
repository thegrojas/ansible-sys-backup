<div align="center">

<p align="center">
  <a href="" rel="noopener">
    <img width="30%" src="./ansible-wordmark.png" alt="Ansible Wordmark"></a>
  </a>
</p>

  [![Status](https://img.shields.io/badge/status-Active-success.svg)]() 
  [![Ansible Galaxy](https://img.shields.io/ansible/role/TODO)]() 
  [![Ansible Version](https://img.shields.io/badge/ansible-TODO-informational)]() 
  [![Ansible Version](https://img.shields.io/ansible/quality/TODO)]() 
  [![License](https://img.shields.io/badge/license-GPLv3-blue.svg)](/LICENSE)

</div>

---

# Ansible System - Backups

Ansible role to set up a fully automated backup system using `borgbackup` and `borgmatic`.

- Installs Borgbackup and Borgmatic.
- Creates the folder structure needed for the configuration files.
- Copies common and host-specific configuration files.
- Initializes Borgmatic repositories.
- Sets up a Cron job to run Borgmatic every week.

## 🦺 Requirements

*None*

## 🗃️ Role Variables

You can set the backups folder and the details for the cron job via variables

**Example:**

Available variables are listed below, along with default values (see `defaults/main.yml`):

```yaml
backups_folder: '/data/backups'
cron_job:
  name: 'Backup every Saturday at 19:00'
  minute: '0'
  hour: '19'
  weekday: '6'
  job: '/usr/local/bin/borgmatic --monitoring-verbosity 1'
```

## 📦 Dependencies

*None*

## 🤹 Example Playbook

At the moment you only need to copy your borgmatic backup file to `roles/sys-backup/files/` it has to be named as `ansible_hostname`. After that just run the playbook and the role will set up borgmatic, copy the configuration file and create the cron job.

*TODO*

## ⚖️ License

This code is released under the [GPLv3](./LICENSE) license. For more information refer to `LICENSE.md`

## ✍️ Author Information

Gerson Rojas <thegrojas@protonmail.com>
